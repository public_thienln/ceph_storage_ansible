
# Deploy new ceph block storage cluster 

![ceph cluster model](images/ceph.png)

### Run main-playbook by using root password for the first time
ansible-playbook -i staging_hosts -u root -k --tags "user" --limit "{hostname}" main-playbook.yaml

### Run main-playbook by using your user and your key
ansible-playbook -i staging_hosts -u ${sshuser} -K --key-file $rsa --tags "user,ssh" --limit "{hostname}" main-playbook.yaml  
ansible-playbook -i staging_hosts -u ${sshuser} -K --key-file $rsa --tags "user,ssh" --limit "{group}" main-playbook.yaml  

## Run full main-playbook to deploy ceph blockstorage cluster 
ansible-playbook -i staging_hosts -u ${sshuser} -K  --key-file $rsa --tags "user,ssh" --limit "{group}" --extra-vars "force_ansible=yes" main-playbook.yaml  

## On admin node 

ceph image (if osd nodes can not pull ceph image from internet, you can redirect ceph image path to admin node)
```
podman pull quay.io/ceph/ceph:v17.2.5
podman tag quay.io/ceph/ceph:v17.2.5 10.10.37.70:5000/ceph/ceph:v17.2.5
podman push 10.10.37.70:5000/ceph/ceph:v17.2.5
```

monitoring image
```
podman pull quay.io/ceph/ceph:v17.2
podman pull quay.io/prometheus/prometheus:v2.33.4
podman pull docker.io/grafana/loki:2.4.0
podman pull docker.io/grafana/promtail:2.4.0
podman pull quay.io/prometheus/node-exporter:v1.3.1
podman pull quay.io/prometheus/alertmanager:v0.23.0
podman pull quay.io/ceph/ceph-grafana:8.3.5
podman pull quay.io/ceph/haproxy:2.3
podman pull quay.io/ceph/keepalived:2.1.5
podman pull docker.io/maxwo/snmp-notifier:v1.2.1
```

## RUN bootstrap cluster
1. 